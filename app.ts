import { JetClient, JetMarket, JetReserve, JET_MARKET_ADDRESS } from '@jet-lab/jet-engine'
import { Provider, Wallet, web3 } from '@project-serum/anchor'
import { Connection, Keypair } from '@solana/web3.js'
import { loadImage } from 'canvas';
import fs from 'fs';
import TwitterApi from 'twitter-api-v2';

const { createCanvas } = require('canvas')

const mainnetTokens = new Map([
  ['EPjFWdd5AufqSSqeM2qN1xzybapC8G4wEGGkZwyTDt1v', 'USDC'],
  ['So11111111111111111111111111111111111111112', 'SOL'],
  ['9n4nbM75f5Ui33ZbPYXn59EwSgE8CGsHtAeTH5YFeJ9E', 'BTC'],
  ['2FPyTwcZLUg1MDrwsyoP4D6s1tM7hAkHYRjkNb5w6Pxk', 'ETH']
])

const JET_WHITE = '#E4EAF3';
const JET_BLACK = '#444444';
const JET_BLUE = '#32A5D3';

const twitterClient = new TwitterApi({
  appKey: '<PUT_HERE_APP_KEY>',
  appSecret: '<PUT_HERE_APP_SECRET>',
  // Following access tokens are not required if you are
  // at part 1 of user-auth process (ask for a request token)
  // or if you want a app-only client (see below)
  accessToken: '<PUT_HERE_ACCESS_TOKEN>',
  accessSecret: '<PUT_HERE_ACESS_SECRET>',
});
const keypair = web3.Keypair.generate();

async function getAPY() {
  // transaction commitment options
  const options = Provider.defaultOptions();
  const connection = new Connection("https://api.mainnet-beta.solana.com", options);
  // A wallet is not required in this example
  const wallet = new Wallet(keypair);
  const provider = new Provider(connection, wallet, options);

  // Load the Anchor IDL
  const client = await JetClient.connect(provider, true);

  // Load market data 
  const market = await JetMarket.load(client, JET_MARKET_ADDRESS);
  // Load all reserves
  const reserves = await JetReserve.loadMultiple(client, market);
  
  // text tweet
  let post = geneateTweetText(reserves);
  console.log(post);
  //await twitterClient.v2.tweet(post);

  // picture tweet
  await createPicturePost(reserves);
  // upload picture to twitter
  const mediaIds = await Promise.all([
    // file path
    twitterClient.v1.uploadMedia('./image.png')
  ]);

  // tweet pic
  await twitterClient.v1.tweet('$USDC $SOL $BTC $ETH', { media_ids: mediaIds });
}


// generate text with provided market data
function geneateTweetText(reserves: JetReserve[]) {
  // header
  let text = 'Current Rates\n'.concat('Market | Avail. Liq. | Deposit | Borrow');

  // asset info
  reserves.forEach(reserve => {
    text = text.concat("\n$" + mainnetTokens.get(reserve.data.tokenMint.toBase58()) + " | ",
      floorNum(reserve.data.availableLiquidity.tokens) + " | ",
      apyConvert(reserve.data.depositApy) + "%"+ " | ",
      apyConvert(reserve.data.borrowApr) + "%")
  });
  return text;
}

function apyConvert(apy: number) {
    return Math.round(apy * 100 * 100) / 100;
}

function floorNum(liq: number) {
  return Math.floor(liq * 100) / 100;
}

// generate picture with provided market data
async function createPicturePost(reserves: JetReserve[]) {
  // pic background
  const width = 1000
  const height = 800
  const canvas = createCanvas(width, height)
  const context = canvas.getContext('2d')

  context.fillStyle = JET_BLACK
  context.fillRect(0, 0, width, height)

  // header
  context.font = 'bold 25pt Poppins SemiBold'
  context.textAlign = 'center'
  context.fillStyle = '#b2b2b2';
  context.fillText('ASSET', 175, 100)
  context.fillText('DEPOSIT RATE', 500, 100)
  context.fillText('BORROW RATE', 800, 100)

  // line below header
  context.beginPath();
  context.lineWidth = 5;
  context.strokeStyle = JET_BLUE;
  context.moveTo(0, 150)
  context.lineTo(width, 150)
  context.stroke();

  // asset pics
  let image = await loadImage('./pictures/USDC.png');
  context.drawImage(image, 60, 185, 100, 100)
  image = await loadImage('./pictures/SOL.png');
  context.drawImage(image, 60, 335, 100, 100)
  image = await loadImage('./pictures/BTC.png');
  context.drawImage(image, 60, 485, 100, 100)
  image = await loadImage('./pictures/ETH.png');
  context.drawImage(image, 60, 485+150, 100, 100)

  // asset data
  context.font = '30pt Poppins SemiBold'
  context.fillStyle = '#f8f8f8';
  let x = 175;
  let y = 250;
  reserves.forEach(reserve => {
    context.textAlign = 'left'
    context.fillText(mainnetTokens.get(reserve.data.tokenMint.toBase58()), x, y)
    context.textAlign = 'center'
    context.fillText(apyConvert(reserve.data.depositApy) + "%", x + 325, y)
    context.fillText(apyConvert(reserve.data.borrowApr) + "%", x + 625, y)
    y = y + 150;
  })

  // generate pic
  const buffer = canvas.toBuffer('image/png')
  fs.writeFileSync('./image.png', buffer)
  console.log("Image generated")
}

getAPY();import { JetClient, JetMarket, JetReserve, JET_MARKET_ADDRESS } from '@jet-lab/jet-engine'
import { Provider, Wallet, web3 } from '@project-serum/anchor'
import { Connection, Keypair } from '@solana/web3.js'
import { loadImage } from 'canvas';
import fs from 'fs';
import TwitterApi from 'twitter-api-v2';

const { createCanvas } = require('canvas')

const mainnetTokens = new Map([
  ['EPjFWdd5AufqSSqeM2qN1xzybapC8G4wEGGkZwyTDt1v', 'USDC'],
  ['So11111111111111111111111111111111111111112', 'SOL'],
  ['9n4nbM75f5Ui33ZbPYXn59EwSgE8CGsHtAeTH5YFeJ9E', 'BTC'],
  ['2FPyTwcZLUg1MDrwsyoP4D6s1tM7hAkHYRjkNb5w6Pxk', 'ETH']
])

const JET_WHITE = '#E4EAF3';
const JET_BLACK = '#444444';
const JET_BLUE = '#32A5D3';

const twitterClient = new TwitterApi({
  appKey: '',
  appSecret: '',
  // Following access tokens are not required if you are
  // at part 1 of user-auth process (ask for a request token)
  // or if you want a app-only client (see below)
  accessToken: '',
  accessSecret: '',
});
const keypair = web3.Keypair.generate();

async function getAPY() {
  // transaction commitment options
  const options = Provider.defaultOptions();
  const connection = new Connection("https://api.mainnet-beta.solana.com", options);
  // A wallet is not required in this example
  const wallet = new Wallet(keypair);
  const provider = new Provider(connection, wallet, options);

  // Load the Anchor IDL
  const client = await JetClient.connect(provider, true);

  // Load market data 
  const market = await JetMarket.load(client, JET_MARKET_ADDRESS);
  // Load all reserves
  const reserves = await JetReserve.loadMultiple(client, market);
  
  let post = geneateTweetText(reserves);
  console.log(post);
  await twitterClient.v2.tweet(post);
  //await createPicturePost(reserves);
}


// generate text with provided market data
function geneateTweetText(reserves: JetReserve[]) {
  // header
  let text = 'Current Rates\n'.concat('Market | Avail. Liq. | Deposit | Borrow');

  // asset info
  reserves.forEach(reserve => {
    text = text.concat("\n$" + mainnetTokens.get(reserve.data.tokenMint.toBase58()) + " | ",
      floorNum(reserve.data.availableLiquidity.tokens) + " | ",
      apyConvert(reserve.data.depositApy) + "%"+ " | ",
      apyConvert(reserve.data.borrowApr) + "%")
  });
  return text;
}

function apyConvert(apy: number) {
    return Math.round(apy * 100 * 100) / 100;
}

function floorNum(liq: number) {
  return Math.floor(liq * 100) / 100;
}

// generate picture with provided market data
async function createPicturePost(reserves: JetReserve[]) {
  // pic background
  const width = 1000
  const height = 800
  const canvas = createCanvas(width, height)
  const context = canvas.getContext('2d')

  context.fillStyle = JET_BLACK
  context.fillRect(0, 0, width, height)

  // header
  context.font = 'bold 25pt Poppins SemiBold'
  context.textAlign = 'center'
  context.fillStyle = '#b2b2b2';
  context.fillText('ASSET', 175, 100)
  context.fillText('DEPOSIT RATE', 500, 100)
  context.fillText('BORROW RATE', 800, 100)

  // line below header
  context.beginPath();
  context.lineWidth = 5;
  context.strokeStyle = JET_BLUE;
  context.moveTo(0, 150)
  context.lineTo(width, 150)
  context.stroke();

  // asset pics
  let image = await loadImage('./pictures/USDC.png');
  context.drawImage(image, 60, 185, 100, 100)
  image = await loadImage('./pictures/SOL.png');
  context.drawImage(image, 60, 335, 100, 100)
  image = await loadImage('./pictures/BTC.png');
  context.drawImage(image, 60, 485, 100, 100)
  image = await loadImage('./pictures/ETH.png');
  context.drawImage(image, 60, 485+150, 100, 100)

  // asset data
  context.font = '30pt Poppins SemiBold'
  context.fillStyle = '#f8f8f8';
  let x = 175;
  let y = 250;
  reserves.forEach(reserve => {
    context.textAlign = 'left'
    context.fillText(mainnetTokens.get(reserve.data.tokenMint.toBase58()), x, y)
    context.textAlign = 'center'
    context.fillText(apyConvert(reserve.data.depositApy) + "%", x + 325, y)
    context.fillText(apyConvert(reserve.data.borrowApr) + "%", x + 625, y)
    y = y + 150;
  })

  // generate pic
  const buffer = canvas.toBuffer('image/png')
  fs.writeFileSync('./image.png', buffer)
  console.log("Image generated")
}

getAPY();
